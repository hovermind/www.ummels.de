module Jekyll
  class ImageTag < Liquid::Tag

    def initialize(tag_name, markup, tokens)
      super
      @markup = markup
    end

    def render(context)
      # Render any liquid variables in tag arguments and unescape template code
      render_markup = Liquid::Template.parse(@markup).render(context).gsub(/\\\{\\\{|\\\{\\%/, '\{\{' => '{{', '\{\%' => '{%')

      # Gather settings
      site = context.registers[:site]
      settings = site.config['image']
      markup = /^(?<src>[^\s]+\.[a-zA-Z0-9]{3,4})\s*(?<html_attr>[\s\S]+)?$/.match(render_markup)

      raise "Image Tag can't read this tag. Try {% image path/to/img.jpg [attr=\"value\"] %}." unless markup
      
      # Assign defaults
      source_url = settings['source'] || '.'
      image_uri = File.join(source_url, markup[:src])
      
      "<img src=\"#{image_uri}\" #{markup[:html_attr]}>"
    end
  end
end

Liquid::Template.register_tag('image', Jekyll::ImageTag)
