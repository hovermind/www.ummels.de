---
date: 2014-11-04 20:20:10+00:00
layout: single
slug: dawn-in-brunswick
title: Dawn in Brunswick
wordpress_id: 448
post_format:
- Image
tags:
- photo
- tram
---

This tram has just left the depot to make its first run as line M5 to Hauptbahnhof. Due to construction work at Leonhardplatz the direct route to the terminal is blocked and all trams need to go via [Schloss](https://en.wikipedia.org/wiki/Brunswick_Palace), which made this picture possible.

{% image PB044499.jpg alt="Tram Leonhardstraße at Dawn" %}
