---
date: 2015-02-22 17:46:20+00:00
layout: single
slug: taking-the-slow-train-through-the-harz-mountains
title: Taking the Slow Train Through the Harz Mountains
wordpress_id: 560
tags:
- railway
- photo
---

Not far away from Braunschweig in the Eastern part of the Harz Mountains lies [a large narrow-gauge railway network](https://en.wikipedia.org/wiki/Harz_Narrow_Gauge_Railways) where steam engines still operate on most services. In order to tour almost the entire network, my colleague and I started our tour in Quedlinburg, which is easily reached by a regional train from Magdeburg. From there, a steam train took us on the [Selke Valley Railway](https://en.wikipedia.org/wiki/Selke_Valley_Railway) to Alexisbad.

{% image P2174608.jpg alt="Selketalbahn" %}

After switching to a diesel car in Alexisbad, we arrived at Eisfelder Talmühle where another steam train took us on the [Trans-Harz Railway](https://en.wikipedia.org/wiki/Harz_Railway) all the way to Drei Annen Hohne, where we changed to the famous [Brocken Railway](https://en.wikipedia.org/wiki/Brocken_Railway).

{% image P2174661.jpg alt="Eisfelder Talmühle" %}

{% image P2174689.jpg alt="Harzquerbahn" %}

Unfortunately, soon after starting the ascent to the Brocken mountain, we entered the clouds so that I could not enjoy the superb view from the mountain over Northern Germany.

{% image P2174737.jpg alt="Brockenbahn" %}

{% image P2174748.jpg alt="Brockenbahnhof" %}

After a water stop in Drei Annen Hohne, our steam train took us on the remaining part of the Trans-Harz Railway to its terminal at Wernigerode, another interchange point with Deutsche Bahn's regional network.

{% image P2174751.jpg alt="Drei Annen Hohne" %}
