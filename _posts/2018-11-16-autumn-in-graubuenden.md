---
layout: single
title: Autumn in Graubünden
tags:
- railway
- photo
- switzerland
---

It's that time of the year when the trees show their nicest colours. What better time to visit the most Eastern part of Switzerland, the Canton of [Graubünden (Grisons)](https://en.wikipedia.org/wiki/Canton_of_Grisons) and its narrow-gauge railway network operated by the [Rhaetian Railway (RhB)](https://en.wikipedia.org/wiki/Rhaetian_Railway)? If you take the train from Freiburg (Germany) at 6 in the morning, it is possible to go as far as Pontresina in the Upper Engadin (or even further South as far as Alp Grüm or so) and return shortly after midnight the following day. So I got up early only to find out that my [Nightjet train](https://en.wikipedia.org/wiki/Nightjet) from Hamburg to Zürich was indefinitely delayed. Fortunately, I could take a regional train to Basel instead and arrived in Basel only 30 minutes later as planned.

{% image PA218839.jpg alt="Basel SBB" %}

After another change in Zürich, I finally reached Graubünden and therefore RhB territory at Landquart.

{% image PA218849.jpg alt="Landquart" %}

For the outward journey, I decided to take the line through the [Vereina tunnel](https://en.wikipedia.org/wiki/Vereina_Tunnel) to Sagliains and change there to the Engadin line towards Samedan. Here you can see my train leaving Klosters shortly before entering the Vereina tunnel.

{% image PA218873.jpg alt="Klosters" %}

From Sagliains, which is famous for its use as a [*connecting station*](https://en.wikipedia.org/wiki/Sagliains_(Rhaetian_Railway_station)), the Engadin line originating in Scuol-Tarasp near the Swiss-Austro-Italian border triangle passes along the Inn. In Autumn, the Engadin line is particularly scenic because of the yellow-coloured Larch trees filling the landscape.

{% image PA218898.jpg alt="Engadin Line" %}

{% image PA218901.jpg alt="Engadin Line" %}

{% image PA218966.jpg alt="Engadin Line" %}

After merging with the [Albula line](https://en.wikipedia.org/wiki/Albula_Railway) from Chur in Bever, the next stop is Samedan, where the line continues straight to St. Moritz.
The train from Scuol-Tarasp however makes a left turn to reach its end point at Pontresina where it meets the [Bernina line](https://en.wikipedia.org/wiki/Bernina_railway) from St. Moritz to Tirano in Italy.

{% image PA218988.jpg alt="Pontresina" %}

Between Pontresina and Punt Muragl, both the Bernina line and the Pontresina line (as the line from Samedan is called here) run parallel to each other, the Pontresina line being powered by RhB's usual 11kV AC while the Bernina line is being powered by 1 kV DC. Here is an Allegra unit carrying a Bernina line train towards Tirano.

{% image PA219011.jpg alt="Bernina Line near Punt Muragl" %}

From Punt Muragl I took the next train to Samedan where I changed to the following InterRegio train on the Albula line to Chur. From Bever, the line runs along the Bever river until reaching the [Albula Tunnel](https://en.wikipedia.org/wiki/Albula_Tunnel), the second highest tunnel through the Swiss Alps (only the [Furka Summit Tunnel](https://en.wikipedia.org/wiki/Furka_Summit_Tunnel) is higher, at Spinas.

{% image PA219022.jpg alt="Albula Line between Bever and Spinas" %}

After leaving the tunnel at Preda, the railway descends to Bergün through the famous *Albula Carousel*, which includes two curved tunnels and three spiral tunnels.

{% image PA219028.jpg alt="Albula Line at Preda" %}

{% image PA219052.jpg alt="Albula Line between Preda and Bergün" %}

{% image PA219053.jpg alt="Albula Line between Preda and Bergün" %}

From Bergün the train heads to Filisur, where you can change to Davos. I got out at Filisur in order to go on a small hike to a viewing point for the famous [Landwasser Viaduct](https://en.wikipedia.org/wiki/Landwasser_Viaduct) further along the line towards Thusis.

{% image PA219085.jpg alt="Landwasser Viaduct" %}

Back on the train I crossed the Landwasser Viaduct myself. As you can see, my train consisted of another Allegra unit (which can cope with both 16 kV AC and 1kV DC) and a couple of the new Alvra coaches. Fortunately for the photographer, those coaches include a photo compartment (filled with Asian tourists) with windows that can be opened.

{% image PA219101.jpg alt="Alvra train on the Landwasser Viaduct" %}

Leaving the Landwasser Viaduct behind, the line follows the eponymous Albula river until its confluence with the Hinterrhein at Thusis.

{% image PA219103.jpg alt="Albula Line between Filisur and Thusis" %}

After Thusis, the line (now simply called the Landquart-Thusis Line) runs parallel to the Hinterrhein to Reichenau, where Hinterrhein and Vorderrhein merge to form the Rhine. Shortly afterwards, the train reaches Chur, the capital of Graubünden, where I changed to an SBB Intercity to Zürich.

{% image PA219145.jpg alt="Chur" %}

{% image PA219148.jpg alt="Chur" %}

From Zürich, another Intercity train brought me to Basel, where I finally caught one of DB's infamous *Intercity night trains* (IC 208 to Kiel via Cologne), which together with ÖBB's NightJet trains have replaced all of the former CityNightLine night trains. Fortunately, there was no need to spend the whole night on that train, since we arrived at Freiburg not even one hour after the departure at Basel SBB.
