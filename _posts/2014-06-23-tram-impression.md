---
date: 2014-06-23 15:36:19+00:00
layout: single
slug: tram-impression
title: Tram Impression
wordpress_id: 421
post_format:
- Image
tags:
- photo
- tram
---

Just a quick shot I took with my Nexus 5 in one of Brunswick's oldest trams, which are due to be replaced later this year.

{% image IMG_20140602_183806.jpg alt="M5 Leonhardplatz" %}
