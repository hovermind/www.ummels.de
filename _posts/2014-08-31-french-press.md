---
date: 2014-08-31 14:42:43+00:00
layout: single
slug: french-press
title: French Press
wordpress_id: 431
tags:
- coffee
- photo
---

Finally, I have decided to get a decent coffee setup for my home. After playing with the thought of buying a real Espresso machine, I finally went for a good grinder (which I could also use with an Espresso machine later) combined with a simple [French press](https://en.wikipedia.org/wiki/French_press), which already gives much better coffee than using a filter with factory ground coffee. I am still on the hunt for the best beans in town, so don't expect a final verdict anytime soon.

{% image P8123616.jpg alt="Coffee setup" %}
